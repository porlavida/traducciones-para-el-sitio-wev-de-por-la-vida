export function getPhrases() {
  return {
    app: {
      header: {
        content: 'Contenido',
        workflow: 'Flujo de trabajo',
        media: 'Medios',
        quickAdd: 'Crear nuevo'
      },
      app: {
        errorHeader: 'Error cargando la configuración del CMS',
        configErrors: 'Errores de configuración',
        checkConfigYml: 'Revisa config.yml',
        loadingConfig: 'Cargando configuración...',
        waitingBackend: 'Esperando por el backend...'
      },
      notFoundPage: {
        header: 'No encontrado'
      }
    },
    collection: {
      sidebar: {
        collections: 'Colecciones',
        searchAll: 'Buscar en todo'
      },
      collectionTop: {
        viewAs: 'Ver como',
        newButton: 'Nueva %{collectionLabel}'
      },
      entries: {
        loadingEntries: 'Cargando entradas',
        cachingEntries: 'Registrando entradas',
        longerLoading: 'Esto puede tardar unos minutos'
      }
    },
    editor: {
      editorControlPane: {
        widget: {
          required: '%{fieldLabel} es obligatorio.',
          regexPattern: "%{fieldLabel} no coincide con el patrón: %{pattern}.",
          processing: '%{fieldLabel}, procesando.',
          range: '%{fieldLabel} debe estar entre %{minValue} y %{maxValue}.',
          min: '%{fieldLabel} debe ser de al menos %{minValue}.',
          max: '%{fieldLabel} debe ser %{maxValue} o menos.'
        }
      },
      editor: {
        onLeavePage: 'Confirma que quieres salir de esta página?',
        onUpdatingWithUnsavedChanges: 'Tienes cambios sin guardar, guárdalos antes de actualizar el estado.',
        onPublishingNotReady: 'Actualiza el estado a "Listo" antes de publicar.',
        onPublishingWithUnsavedChanges: 'Tienes cambios sin guardar, guárdalos antes de publicar.',
        onPublishing: '¿En serio quieres publicar esta entrada?',
        onDeleteWithUnsavedChanges: '¿En serio quieres eliminar esta entrada publicada y tus cambios de la sesión actual?',
        onDeletePublishedEntry: '¿En serio quieres eliminar esta entrada publicada?',
        onDeleteUnpublishedChangesWithUnsavedChanges: 'Esto eliminara todos los cambios no publicados en esta entrada, así como los cambios no guardados de la sesión actual. ¿En serio quieres continuar?',
        onDeleteUnpublishedChanges: 'Todos los cambios no publicados de esta entrada serán eliminados, ¿quieres continuar?',
        loadingEntry: 'Cargando entrada...',
        confirmLoadBackup: 'Un respaldo local de esta entrada ha sido recuperado, ¿quieres usarlo?'
      },
      editorToolbar: {
        publishing: 'Publicando...',
        publish: 'Publicar',
        published: 'Publicado',
        publishAndCreateNew: 'Publicar y crear nuevo',
        deleteUnpublishedChanges: 'Descartar cambios no publicados',
        deleteUnpublishedEntry: 'Descargar entrada no publicada',
        deletePublishedEntry: 'Eliminar entrada publicada',
        deleteEntry: 'Borrar entrada',
        saving: 'Guardando...',
        save: 'Guardar',
        deleting: 'Borrando...',
        updating: 'Actualizando...',
        setStatus: 'Configurar estado',
        backCollection: ' Escribiendo en la colección %{collectionLabel}',
        unsavedChanges: 'Cambios sin guardar',
        changesSaved: 'Cambios guardados',
        draft: 'Borrador',
        inReview: 'En revisión',
        ready: 'Listo',
        publishNow: 'Publicar ahora',
        deployPreviewPendingButtonLabel: 'Revisar vista previa',
        deployPreviewButtonLabel: 'Vista previa',
        deployButtonLabel: 'Ver en vivo'
      },
      editorWidgets: {
        unknownControl: {
          noControl: "‘Widget’ sin ajustes: '%{widget}'."
        },
        unknownPreview: {
          noPreview: "‘Widget’ sin previsualización': %{widget}'."
        }
      }
    },
    mediaLibrary: {
      mediaLibrary: {
        onDelete: '¿En serio quieres eliminar el medio seleccionado?'
      },
      mediaLibraryModal: {
        loading: 'Cargando...',
        noResults: 'Sin resultados.',
        noAssetsFound: 'Recursos no encontrados.',
        noImagesFound: 'Imágenes no encontradas.',
        private: 'Privado ',
        images: 'Imágenes',
        mediaAssets: 'Recursos multimedia',
        search: 'Buscar...',
        uploading: 'Cargando...',
        uploadNew: 'Cargar nuevo',
        deleting: 'Elimininando...',
        deleteSelected: 'Eliminar seleccionado',
        chooseSelected: 'Elegir seleccionado'
      }
    },
    ui: {
      errorBoundary: {
        title: 'Error',
        details: "Se ha encontrado un error - por favor ",
        reportIt: 'repórtelo.',
        detailsHeading: 'Detalles',
        recoveredEntry: {
          heading: 'Documento recuperado',
          warning: '¡Por favor copie esto antes de continuar!',
          copyButtonLabel: 'Copiar a porta papeles'
        }
      },
      settingsDropdown: {
        logOut: 'Cerrar sesión'
      },
      toast: {
        onFailToLoadEntries: 'Fallo al cargar la entrada: %{details}',
        onFailToLoadDeployPreview: 'Fallo al cargar vista previa: %{details}',
        onFailToPersist: 'Fallo al almacenar la entrada: %{details}',
        onFailToDelete: 'Fallo al eliminar la entrada: %{details}',
        onFailToUpdateStatus: 'Fallo al actualizar estado: %{details}',
        missingRequiredField: "Te faltó rellenar al menos un campo obligatorio, por favor revisa el formulario antes de continuar.",
        entrySaved: 'Entrada guardada',
        entryPublished: 'Entrada publicada',
        onFailToPublishEntry: 'Fallo al publicar: %{details}',
        entryUpdated: 'Estado de la entrada actualizada',
        onDeleteUnpublishedChanges: 'Se descartaron los cambios no publicados',
        onFailToAuth: '%{details}'
      }
    },
    workflow: {
      workflow: {
        loading: 'Carganod entradas del flujo de trabajo editorial',
        workflowHeading: 'Flujo de trabajo editorial',
        newPost: 'Nueva entrada',
        description: '%{smart_count} entrada en espera de revisión, %{readyCount} lista para lanzar. |||| %{smart_count} entradas en espera de revisión, %{readyCount} listas para lanzar. '
      },
      workflowCard: {
        lastChange: '%{date} por %{author}',
        deleteChanges: 'Descartar cambios',
        deleteNewEntry: 'Eliminar entrada nueva',
        publishChanges: 'Publicar cambios',
        publishNewEntry: 'Publicar nueva entrada'
      },
      workflowList: {
        onDeleteEntry: '¿En serio quieres eliminar esta entrada?',
        onPublishingNotReadyEntry: 'Solo los elementos con el estado "Listo" pueden publicarse. Arrastra la tarjeta a la columna "Listo" para habilitar la publicación',
        onPublishEntry: '¿En serio quieres publicar esta entrada?',
        draftHeader: 'Borradores',
        inReviewHeader: 'En revisión',
        readyHeader: 'Listo',
        currentEntries: '%{smart_count} entrada |||| %{smart_count} entradas'
      }
    }
  };
}
