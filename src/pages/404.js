import React from 'react'
import Layout from '../components/Layout'

const NotFoundPage = () => (
  <Layout>
    <div>
      <h1>No encontrado</h1>
      <p>Error 404. El recurso solicitado no se ha encontrado en la ubicación indicada, sírvase utilizar la caja de búsqueda adjunta.</p>
    </div>
  </Layout>
)

export default NotFoundPage
